package Components;

public class TimerThread extends Thread {
    private Heater heater;
    private TemperatureSensor temperatureSensor;
    private int targetTemperature;

    public TimerThread(Heater heater,
                       TemperatureSensor temperatureSensor,
                       int targetTemperature){
        super();
        this.heater = heater;
        this.temperatureSensor = temperatureSensor;
        this.targetTemperature = targetTemperature;
    }

    @Override
    public void run(){
        int heatingTimer = 3;
        heater.setRunning(true);
        int chunk = 0;
        if (temperatureSensor != null){
            int currentTemperature = temperatureSensor.getTemp();
            int diff = targetTemperature - currentTemperature;
            chunk = diff/heatingTimer;
        }
        while (heater.getTimer()>0){
            if (heatingTimer>0) {
                temperatureSensor.setTemperature(temperatureSensor.getTemp()+chunk);
                System.out.println("Current temperature: "+temperatureSensor.getTemp());
                heatingTimer--;
                if (heatingTimer==0){
                    System.out.println("Heating finished.");
                    temperatureSensor.setTemperature(targetTemperature);
                }
            }
            else {
                System.out.println("Time remaining: " + heater.getTimer());
                heater.setTimer(heater.getTimer()-1);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        heater.setRunning(false);

        System.out.println("Brewing finished.");
    }
}
