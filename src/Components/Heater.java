package Components;

public class Heater {
    private int desiredTemperature;
    private int timer;
    private boolean running;

    public void setTemp(int temperature) {
        this.desiredTemperature = temperature;
    }

    public void setTimer(int time) {
        this.timer = time;
    }

    public void setRunning(boolean isRunning) {
        this.running = isRunning;
    }

    public boolean isRunning() {
        return running;
    }

    public int getTimer() {
        return timer;
    }
}
