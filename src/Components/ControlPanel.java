package Components;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class ControlPanel {
    private Heater heater;
    private TemperatureSensor temperatureSensor;
    private Recipes recipes;
    private Nozzle nozzle;

    private boolean overrideTemperature;
    private int overrideTemperatureValue;
    private boolean overrideBrewingTime;
    private int overrideBrewingTimeValue;

    public ControlPanel(Heater heater,
                        TemperatureSensor temperatureSensor,
                        Recipes recipes,
                        Nozzle nozzle) {
        this.heater = heater;
        this.temperatureSensor = temperatureSensor;
        this.recipes = recipes;
        this.nozzle = nozzle;
    }

    public void startHeater(int temperature, int time) {
        System.out.println("Control panel: heater started.");
        heater.setTimer(time);
        heater.setTemp(temperature);
        TimerThread timerThread = new TimerThread(heater, temperatureSensor, temperature);
        timerThread.start();
        try {
            timerThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    public void executeStep(int recipeId, int step) {
        System.out.println("Starting step " + (step + 1));
        RecipeStep recipeStep = recipes.getRecipe(recipeId).getRecipeStep(step);
        int temp = overrideTemperature ? overrideTemperatureValue : recipes.getRecipe(recipeId).getRecipeStep(step).getTemperature();
        int brew = overrideBrewingTime ? overrideBrewingTimeValue : recipes.getRecipe(recipeId).getRecipeStep(step).getBrewingTime();

        List<Ingredient> additivesList = recipeStep.getAdditivesList();
        if (!additivesList.isEmpty()) System.out.println("Adding ingredients");
        for (int i = 0; i < additivesList.size(); i++) {
            System.out.println(additivesList.get(i).getName());
        }
        System.out.println("Brewing time: " + brew);
        System.out.println("Temperature: " + temp);
        startHeater(temp,brew);
        System.out.println("Step finished.");
    }
    public void executeRecipe(int recipeId) {
        int listSize = recipes.getRecipe(recipeId).getRecipeStepSize();
        for (int i=0; i<listSize; i++){
            changesTemp(recipeId,i);
            changesBrew(recipeId,i);
            executeStep(recipeId,i);
        }
        changesIng();
        nozzle.dispense();
        System.out.println("Recipe finished.");
    }

    public Recipes getRecipes() {
        return recipes;
    }

    public void setOverrideTemperature(boolean override) {
        this.overrideTemperature = override;
    }

    public void setOverrideTemperatureValue(int temperatureValue) {
        this.overrideTemperatureValue = temperatureValue;
    }

    public void setOverrideBrewingTime(boolean overrideBrewingTime) {
        this.overrideBrewingTime = overrideBrewingTime;
    }

    public void setOverrideBrewingTimeValue(int brewingTimeValue) {
        this.overrideBrewingTimeValue = brewingTimeValue;
    }
    public void menuLoop() {
        boolean inLoop = true;
        while (inLoop){
            System.out.println("Select option:");
            System.out.println("1. Select recipe.");
            System.out.println("2. Exit.");
            Scanner scanner = new Scanner(System.in);
            int option = 0;
            try {
                option = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid option.");
                continue;
            }
            switch (option){
                case 1:
                    selectRecipe();
                    break;

                case 2:
                    inLoop = false;
                    break;

                default:
                    System.out.println("Invalid option.");
                    break;
            }
        }
    }

    private void selectRecipe() {
        int size = getRecipes().getSize();
        System.out.println("Select recipe:");
        for (int i = 0; i<size; i++){
            System.out.printf("%d: %s%n",i+1, getRecipes().getRecipe(i).getName());
        }

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt()-1;
        if (option>=0 && option<size) {
            System.out.println("Ingredients:");
            Recipe selectedRecipe = getRecipes().getRecipe(option);
            for (int i = 0; i<selectedRecipe.getIngredientSize(); i++) {
                System.out.println(selectedRecipe.getIngredient(i).getName());
            }

            executeRecipe(option);

        }
        else {
            System.out.println("Invalid option.");
        }
    }

    private void changesTemp(int option, int stepId) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to change temperature? (y/n) Current temperature: "+
                getRecipes().getRecipe(option).getRecipeStep(stepId).getTemperature());
        String optionTemp = scanner.nextLine();
        switch (optionTemp) {
            case "y":
                setOverrideTemperature(true);
                System.out.println("Desired temperature:");
                int temperatureNew = 0;
                try {
                    temperatureNew = scanner.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("Invalid option.");
                    changesTemp(option, stepId);
                    break;
                }
                if (temperatureNew<=0){
                    setOverrideTemperature(false);
                }
                else setOverrideTemperatureValue(temperatureNew);

                break;

            case "n":
                setOverrideTemperature(false);
                break;

            default:
                System.out.println("Invalid option.");
                changesTemp(option, stepId);
                break;
        }

    }

    private void changesBrew(int option, int stepId) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to change brewing time? (y/n) Current brewing time: "+
                getRecipes().getRecipe(option).getRecipeStep(stepId).getBrewingTime());
        String optionBrew = scanner.nextLine();
        switch (optionBrew) {
            case "y":
                setOverrideBrewingTime(true);
                System.out.println("Desired brewing time:");
                int brewNew = 0;
                try {
                    brewNew = scanner.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("Invalid option.");
                    changesBrew(option, stepId);
                    break;
                }
                if (brewNew<=0){
                    setOverrideBrewingTime(false);
                }
                else setOverrideBrewingTimeValue(brewNew);

                break;

            case "n":
                setOverrideBrewingTime(false);
                break;

            default:
                System.out.println("Invalid option.");
                changesBrew(option, stepId);
                break;
        }

    }
    private void changesIng() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to add another ingredient? (y/n)");
        String optionIng = scanner.nextLine();
        switch (optionIng) {
            case "y":
                System.out.println("OK, add your ingredient.");
                break;

            case "n":

                break;

            default:
                System.out.println("Invalid option.");
                changesIng();
                break;
        }

    }
}
