package Components;

import java.util.List;
public class Recipe {
    private List<Ingredient> ingredients;
    private String name;
    private List<RecipeStep> recipeSteps;

    public Recipe(String name,
                  List<Ingredient> ingredients,

                  List<RecipeStep> recipeSteps) {
        this.name = name;
        this.ingredients = ingredients;

        this.recipeSteps = recipeSteps;
    }

    public Ingredient getIngredient(int index) {
        return ingredients.get(index);
    }

    public int getIngredientSize(){
        return ingredients.size();
    }


    public String getName() {
        return name;
    }

    public RecipeStep getRecipeStep(int step) {
        return recipeSteps.get(step);
    }
    public int getRecipeStepSize(){
        return recipeSteps.size();
    }
}
