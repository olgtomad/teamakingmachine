package Components;

import java.util.ArrayList;
import java.util.List;

public class RecipeStep {
    private List<Ingredient> additivesList;
    private int temperature;
    private int brewingTime;

    public RecipeStep(List<Ingredient> additivesList,
                      int temperature,
                      int brewingTime) {
        this.additivesList = additivesList;
        this.temperature = temperature;
        this.brewingTime = brewingTime;
    }
    public RecipeStep(int temperature,
                      int brewingTime) {
        this.temperature = temperature;
        this.brewingTime = brewingTime;
        this.additivesList = new ArrayList<>();
    }

    public List<Ingredient> getAdditivesList() {
        return additivesList;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getBrewingTime() {
        return brewingTime;
    }
}
