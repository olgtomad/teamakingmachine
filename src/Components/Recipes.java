package Components;

import java.util.ArrayList;
import java.util.List;

public class Recipes {
    private List<Recipe> recipeList = new ArrayList<>();

    public int getSize() {
        return recipeList.size();
    }

    public Recipe getRecipe(int id) {
        return recipeList.get(id);
    }

    public void addRecipe(Recipe recipe) {
        recipeList.add(recipe);
    }
}
