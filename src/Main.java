import Components.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Let's make some tea!");
        Ingredient ingredient1 = new Ingredient("Black Tea leaves");
        Ingredient ingredient2 = new Ingredient("Addition - orange peel");
        Ingredient ingredient3 = new Ingredient("Water");
        Ingredient ingredient4 = new Ingredient("White Tea leaves");
        Ingredient ingredient5 = new Ingredient("Addition - cinnamon");
        List<Ingredient> ingredientList = new ArrayList<>();
        ingredientList.add(ingredient1);
        ingredientList.add(ingredient2);
        ingredientList.add(ingredient3);
        List<Ingredient> additivesList = new ArrayList<>();
        additivesList.add(ingredient2);

        List<RecipeStep> recipeSteps = new ArrayList<>();

        RecipeStep recipeStep = new RecipeStep(90,5);
        recipeSteps.add(recipeStep);
        recipeStep = new RecipeStep(additivesList,80,10);
        recipeSteps.add(recipeStep);

        Recipe recipe = new Recipe("Orange flavored tea.", ingredientList, recipeSteps);


        List<Ingredient> ingredientList2 = new ArrayList<>();
        ingredientList2.add(ingredient4);
        ingredientList2.add(ingredient3);
        ingredientList2.add(ingredient5);
        List<Ingredient> additivesList2 = new ArrayList<>();
        additivesList2.add(ingredient5);

        List<RecipeStep> recipeSteps2 = new ArrayList<>();

        RecipeStep recipeStep2 = new RecipeStep(90,5);
        recipeSteps2.add(recipeStep2);
        recipeStep2 = new RecipeStep(additivesList,80,10);
        recipeSteps2.add(recipeStep2);
        recipeStep2 = new RecipeStep(additivesList,60,10);
        recipeSteps2.add(recipeStep2);

        Recipe recipe2 = new Recipe("Cinnamon tea.", ingredientList2, recipeSteps2);

        Recipes recipes = new Recipes();
        recipes.addRecipe(recipe);
        recipes.addRecipe(recipe2);

        Heater heater = new Heater();
        TemperatureSensor temperatureSensor = new TemperatureSensor();
        Nozzle nozzle = new Nozzle();
        ControlPanel controlPanel = new ControlPanel(heater, temperatureSensor, recipes, nozzle);
        controlPanel.menuLoop();
    }


}